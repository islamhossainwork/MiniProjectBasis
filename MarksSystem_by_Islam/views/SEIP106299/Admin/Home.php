<?php
function __autoload($classname){
        $file =str_replace("\\","/",$classname);  
        include ("../../../".$file.'.php');
     }

     use src\BITM\SEIP106299\Admin\Batch;

     $e_batch = new Batch();
     $total_batch = $edit_batch = $e_batch->Home_batch();
     
     $total_student = $edit_batch = $e_batch->Home_student();

     
    session_start();
    if(isset($_SESSION['user_email'])){       
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Trainee Maintenance System</title>

    <!-- Bootstrap -->
    <link href="../../../css/bootstrap.min.css" rel="stylesheet">
    <!-- My selected style -->
    <link href="../../../css/style.css" rel="stylesheet">

  </head>
  <body>
      <div class="container-fluid">
          <div class="container">
              <h1>
                  <small><img src="../../../image/icon.png" alt="Basis_LOGO" height="20%" width="10%"></small>
                  <header>Trainee Maintenance System</header>
                  <small id="logout_btn"><a class="btn btn-primary" href="logout.php">Logout</a></small>
              </h1>
          </div>          
      </div>
      <!-- header section end -->
      <div class="container">
          <div class="nav">
              <div class="col-md-6"></div>
              <div class="col-md-6">
              <nav>
                  <div class="col-md-2"></div>
                  <div class="col-md-3"><a class="" href="Home.php">Home</a></div>
                  <div class="col-md-3"><a class="" href="Show_batch.php">Show Batch</a></div>
<!--                  <div class="col-md-2"><a class="" href="../Exam/Show_exam_batch.php">Exam</a></div>
                  <div class="col-md-2"><a class="" href="">Show Result</a></div>
                  <div class="col-md-2"><a class="" href="">Tutorial</a></div>                  -->
              </nav>
              </div>
          </div>
          <div class="content">
              <div class="col-md-2"></div>
              <div class="col-md-8">
                <p>
                    <br/>
                    <br/>
               <center>
                   <u><h2>BITM Trainee Maintenance System</h2></u>
                <table class="table-responsive">
                    <thead>
                        <tr>
                            <th id="home_tb_th">Total Batch</th><td> <?php  echo $total_batch; ?></td>
                       </tr>
                        <tr>
                            <th  id="home_tb_th">Total Student</th><td><?php  echo $total_student; ?></td>
                       </tr>

                    </thead>
                </table>
                </center>
              </p>
          </div>
              
          </div><!--content close-->  
      </div>
      <!-- container section end -->
      <div class="container-fluid">
          <div class="container">
               <div class="col-md-4"><br/>Developed By-<a href="">Coder'sCore</a></div>
              <div class="col-md-4"><br/>copyright &copy 2016</div>
              <div class="col-md-4"><br/>Powered By- Basis</div>            
          </div> 
      </div>
      <!-- footert section end -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
<?php
    
    }else{
        echo "<script>alert('You Are Not A Valid User !');</script>";
        echo "<script>window.location='../../../index.php';</script>";
    }
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PhoneBook_by-Islam</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!--my sytle link-->
    <link href="css/style.css" rel="stylesheet" type="text/css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      <!--body field start-->
      <div class="container-fluid">
          <div class="container">
              <h2>Registration Project<br/><small>ID:106299</small></h2>
          </div>
      </div>
          <div class="container">
              <div class="section">   
                  

                  
                  
<!-- Login form..  -->
<div class="col-md-1"></div>
<div class="col-md-4">
<form action="views/SEIP106299/User/login.php" method="post">  
    <b id="form-head"><center>Login now !</b></center><Br/><br/>
                      <div class="form-group">
                        <label for="exampleInputText1">User Name</label>
                        <input type="text" name='username' class="form-control" id="exampleInputText1" placeholder="Enter Username">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Password</label>
                        <input type="password" name='password' class="form-control" id="exampleInputEmail1" placeholder="Your Password">
                      </div>
                        <center><button type="submit" class="btn btn-default">Login</button></center>
</form>
</div>
<!-- Login form end..  -->
<div class="col-md-1"></div>
<!-- registration form..  -->
<div class="col-md-6">
                <form action="views/SEIP106299/User/register.php" method="post">
                        <b id="form-head"><center>Register as new !</b></center><Br/><br/>
                      <div class="form-group">
                        <label for="exampleInputText1">User Name</label>
                        <input type="text" name="username" class="form-control" id="exampleInputText1" placeholder="Enter Username">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputNumber1">User Email</label>
                        <input type="email" name="useremail" class="form-control" id="exampleInputText2" placeholder="Enter Your Email">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputText1">Father's Name</label>
                        <input type="text" name="father_name" class="form-control" id="exampleInputText2" placeholder="Enter Your Father's Name">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputText1">Mother's Name</label>
                        <input type="text" name="mother_name" class="form-control" id="exampleInputText2" placeholder="Enter Your Mother's Name">
                      </div>
                      <div class="form-group">
                        <label for="address">Address:</label>
                        <textarea name="address" class="form-control" rows="5" id="comment" placeholder='Enter Your Address'></textarea>
                      </div>
                      <div class="form-group">
                          <label for="sel1">Education Level:</label>
                          <select name="education" class="form-control" id="sel1">
                            <option>Select Your Eduction Level</option>
                            <option>SSC</option>
                            <option>HSC</option>
                            <option>BSc</option>
                            <option>MSc.</option>
                          </select>
                       </div>
                      <div class="form-group">
                          <label for="sel1">Select Your Gender:</label>&nbsp;&nbsp;&nbsp;
                        <label class="radio-inline"><input type="radio" value='Male' name="gender">Male</label>
                        <label class="radio-inline"><input type="radio" value="Female" name="gender">Female</label>
                       </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Password</label>
                        <input type="password" name="userpassword" class="form-control" id="exampleInputEmail1" placeholder="Your Password">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Confirm Password</label>
                        <input type="password" name="confirmpassword" class="form-control" id="exampleInputEmail1" placeholder="confirm your password">
                      </div>
                        <center><button type="submit" class="btn btn-default">Register</button></center>
                    </form><br/>
</div>
<!-- registration form..  -->
            </div>
          </div>
      <div class="container-fluid">
          <div class="container">
              <div class="header">
                  <div class='col-md-4'>
                      <br/>
                      <p id='left'>copyright&copy2016</p>
                  </div>
                  <div class='col-md-4'>
                      <br/><center><img src='img/icon.png' alt='icon_file_name' height="100%" width='100%'></center>
                  </div>
                  <div class='col-md-4'>
                      <br/>
                      <p id='right'>Powered By-BITM(Basis)<br/>
                             Developed By- Islam Hossain</p>
                  </div>
              </div>
          </div>
      </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>